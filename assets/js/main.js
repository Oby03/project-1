$(function () {
    $(".modalTrigger").on("click", function () {
        $username = $(this).attr('data-user-name');
        $user_id = $(this).attr('data-user-id');
        $('.user_name').html($username);
        $('.to_id').val($user_id);
        $('.to_name').val('@' + $username);
        $('#messageModal').modal('show');
    });

    $("#sendMessage").on("click", function () {
        $data = $("#sendForm").serialize();
        var newAjax = $.ajax({
            url: 'components/send_message.php',
            type: "POST",
            cache: false,
            data: $data,
            dataType: 'json',
            timeout: 2000,
            success: function (data) {
                if (data.success) {
                    $("#message-success").fadeIn(1000);
                    setTimeout(function () {
                        $("#message-success").fadeOut(1000);
                        $('#message-text').val('');
                    }, 1500);
                } else {
                    console.error(data);
                }
            },
            error: function () {
                console.error('failed sending request');
            }
        });
    })
});