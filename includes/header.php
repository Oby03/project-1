<?php 
session_start();
include_once('dbconfig.php');
?>


<!DOCTYPE html>
<html lang="en">
<head>
<style>
*{
    margin:0;
    padding:0;
}
body{
    width:100%;
}
.container{
    width:100%;`
}
</style>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>AdShare</title>
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/main.css">
    <link rel="stylesheet" href="assets/css/Profile-Edit-Form.css">
    <link href="assets/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/profile.css">
    <script src="assets/js/jquery-3.3.1.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    
</head>
<body>

<div class="header">
    <nav class="navbar navbar-inverse" role="navigation">
        <div class="navbar-header"> 
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#example-navbar-collapse"> 
            <span class="sr-only">Adverts</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button> 
            <a class="navbar-brand" href="index.php">AdShare</a>
        </div>
        <div class="collapse navbar-collapse" id="example-navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="home.php">Home</a></li>
                    <li><a href="view_edit_post.php">My Posts</a></li>
                    <li><a href="post.php">Add Post</a></li>
                    <li class="dropdown" style="float:right;"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <?php echo isset($_SESSION['username']) ? $_SESSION['username'] : "";?> <b class="caret"></b> </a> 
                        <ul class="dropdown-menu">
                            <li><a href="myprofile.php">Profile</a></a></li>
                            <li><a href="resetpass.php">Reset password</a></li>
                            <li><a href="logout.php">Log Out</a></li>
                        </ul> 
                    </li>
                </ul>
                <!-- <form class="navbar-form navbar-right" role="search" method="post" autocomplete="off" action="search.php" >
                    <div class="form-group">
                        <input type="text" class="search form-control" id="searchbox" placeholder="Search for category" name="filter"/><br />
                        <!-- <div id="display"></div> -->
                    <!-- </div> 
                    <button type="submit" name="search" class="btn btn-default btn-success"><i class="fa fa-search"></i></button>
				</form>   --> -->
        </div>
         
    </nav>
</div>





