<?php
session_start();
if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
    header("location:signin.php");
}


include_once('includes/header.php');
        if(isset($_POST['submit'])){
    $phone = trim($_POST['phone']);
    $location =trim($_POST['location']);
    $shortbio = trim($_POST['shortbio']);
    $id =$_SESSION['userid'];
    $avatar = $_FILES['avatar'];
    // $avatar = $_POST['avatar'];
    $userid = $_SESSION['userid'];
    

    $avatar_err =$_FILES["avatar"]["error"];
    $filename = $_FILES["avatar"]["name"];
    $filetype = $_FILES["avatar"]["type"];
    $filesize = $_FILES["avatar"]["size"];
    $path ="avatar/" . $_FILES["avatar"]["name"];

    if(isset($avatar) && $avatar_err == 0){
        $allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "gif" => "image/gif", "png" => "image/png");
   
        // Verify file extension
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        if(!array_key_exists($ext, $allowed)) die("Error: Please select a valid file format.");

        // Verify file size - 5MB maximum
        $maxsize = 7 * 1024 * 1024;
        if($filesize > $maxsize) die("Error: File size is larger than the allowed limit.");
    
        // Verify MYME type of the file
        if(in_array($filetype, $allowed)){

            // Check whether file exists before uploading it
            $path ="avatar/" . $_FILES["avatar"]["name"];   
            if(file_exists($path)){
                echo $_FILES["avatar"]["name"] . " it already exists.";
                return;
            } else{
                move_uploaded_file($_FILES["avatar"]["tmp_name"],$path);
                
                echo "Your file was uploaded successfully.";
            } 
        } else{
            echo "Error: There was a problem uploading your file. Please try again."; 
        }
    } else{
        echo "Error: " . $_FILES["avatar"]["error"];
    }
    
echo $path . $phone .$location .$shortbio ;
    $sql =mysqli_query($dbconn," UPDATE users SET avatar= '$path', phone = '$phone ', location ='$location', shortbio = '$shortbio' WHERE ID = $userid");
   if($sql){
    $_SESSION['success'] = "The data was successfully submitted";
       header("location:myprofile.php");
      
   }else{
       echo mysqli_error($dbconn);
   }
}

 ?>

 <div class="container profile profile-view" id="profile">
        <div class="row">
            <div class="col-md-12 alert-col relative">
                <div class="alert alert-info absolue center" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><span>Profile save with success</span></div>
            </div>
        </div>
        <form method="post" enctype="multipart/form-data">
            <div class="form-row profile-row">
                <div class="col-md-4 relative">
                    <div class="avatar">
                        <div class="avatar-bg center"></div>
                    </div><input type="file" class="form-control" name="avatar"></div>
                <div class="col-md-8">
                    <h1>Update Profile </h1>
                    <hr>
                    <div class="form-row">
                        <div class="col-sm-12 col-md-6">
                            <div class="form-group"><label>Firstname </label><input class="form-control" type="text" name="fname"></div>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <div class="form-group"><label>Lastname </label><input class="form-control" type="text" name="lname"></div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-sm-12 col-md-6">
                            <div class="form-group"><label>Location</label><input class="form-control" type="text" name="location"></div>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <div class="form-group"><label>Phone</label><input class="form-control" type="text" name="phone"></div>
                        </div>
                    </div>
                    <div class="form-group"><label>Email </label><input class="form-control" type="email" autocomplete="off" required="" name="email"></div>
                    <div class="form-group"><label>Shortbio</label><textarea class="form-control" name="shortbio"></textarea></div>
                    <hr>
                    <div class="form-row">
                        <div class="col-md-12 content-right"><button class="btn btn-primary form-btn" type="submit" name="submit">SAVE </button><button class="btn btn-danger form-btn" type="reset">CANCEL </button></div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- <div class="row" id="updatepro">
    <div class="col-md-4"></div>
    <div class="col-md-4">
        <div class="profile" style="background: white; box-shadow: 2px 2px; margin:0 auto; padding: 15px; ">
            <h3>Update Profile</h3><br>
            <form action="" method="post" enctype="multipart/form-data">

            <div class="form-group float-label-control">
            <label for="">Profile Picture</label>
            <center><input name="avatar"  class="btn btn-primary ladda-button" data-style="zoom-in"  type="file"/></center>
            </div> 


            <div class="form-group float-label-control">
                <label for="">Location</label>
                <input type="text" name="location" id="" placeholder="" class="form-control">
            </div>
            <div class="form-group float-label-control">
                <label for="">Phone</label>
                <input type="" name="phone" id="" class="form-control"  maxlength="10" >
            </div>

            <div class="form-group float-label-control">
            <label for="">Short Bio</label>
            <textarea class="form-control" rows="10" name="shortbio" value=""></textarea>
        </div>

            <button type="submit" name="submit" class="btn btn-primary">Save Details</button>
        </form>
        </div>
        
    </div>
</div>

 -->
