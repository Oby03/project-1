-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 06, 2018 at 05:23 PM
-- Server version: 5.7.14
-- PHP Version: 7.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `adshare`
--
CREATE DATABASE IF NOT EXISTS `adshare` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `adshare`;

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `ID` int(11) NOT NULL,
  `User_Name` varchar(255) NOT NULL,
  `firstname` text NOT NULL,
  `lastname` text NOT NULL,
  `email` text NOT NULL,
  `phone` int(15) NOT NULL,
  `Password` varchar(255) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`ID`, `User_Name`, `firstname`, `lastname`, `email`, `phone`, `Password`, `created_date`) VALUES
(1, 'admin', '', '', '', 0, '$2y$10$XRpZxsVaKgm4yAtsaNeT2eU9SR3jQfFgX.swrRFpeazjcgYVChy.q', '2018-05-10 13:29:36');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories` (
  `ID` int(11) NOT NULL,
  `Category_Name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`ID`, `Category_Name`) VALUES
(1, 'music'),
(11, 'gghfg'),
(12, 'Books'),
(13, 'Name'),
(14, 'THe name');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
CREATE TABLE `messages` (
  `id` int(10) NOT NULL,
  `from_userid` int(11) DEFAULT NULL,
  `to_userid` int(11) NOT NULL,
  `message_text` text NOT NULL,
  `added_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `from_userid`, `to_userid`, `message_text`, `added_at`) VALUES
(1, 0, 25, '', '2018-06-05 21:23:53'),
(2, 0, 25, '', '2018-06-05 21:24:59'),
(3, 0, 25, 'These are there				', '2018-06-05 21:25:16'),
(4, 0, 25, 'These are there				', '2018-06-05 21:26:22'),
(5, 0, 25, 'Hello man, how are you?				', '2018-06-06 19:01:39'),
(6, 0, 25, 'My name is pozy				', '2018-06-06 19:03:47'),
(7, 0, 26, 'Hello admin, this is pozy!				', '2018-06-06 19:04:47'),
(8, 0, 25, 'hiedss				', '2018-06-06 19:05:21'),
(9, 0, 26, 'Hello I\'M POZY CAN GET SOME HELP?\r\nMy email is polycarpmasika at yahoo dot com				', '2018-06-06 20:20:50');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
CREATE TABLE `posts` (
  `ID` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `Photo` varchar(255) NOT NULL,
  `Description` mediumtext NOT NULL,
  `category_id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `Time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`ID`, `Title`, `Photo`, `Description`, `category_id`, `userid`, `Time`) VALUES
(1, 'titel', 'uploads/Screenshot (4).png', 'njjbi', 1, 21, '2018-05-10 10:40:42'),
(7, 'The name of names', 'uploads/-130248410.jpg', 'The nameThe name of names of names', 12, 21, '2018-05-11 08:45:31'),
(8, 'nii', 'uploads/591848120.jpg', 'jbpiv', 1, 25, '2018-05-11 12:22:58'),
(9, 'hfhhf', 'uploads/1655435576.jpg', 'car is fine', 13, 25, '2018-05-12 17:09:31'),
(10, 'gdgd', 'uploads/3d_Effect-wallpaper-9612728.jpg', 'this is the first post', 12, 25, '2018-05-14 05:34:11'),
(11, 'The Story Of Adidon', 'uploads/1495033492061.jpg', 'House for sale', 1, 26, '2018-06-04 17:11:22');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `ID` int(11) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `avatar` varchar(255) NOT NULL,
  `phone` int(15) NOT NULL,
  `location` varchar(255) NOT NULL,
  `joindate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `User_Name` varchar(255) NOT NULL,
  `Password` varchar(255) NOT NULL,
  `shortbio` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`ID`, `firstname`, `lastname`, `email`, `avatar`, `phone`, `location`, `joindate`, `User_Name`, `Password`, `shortbio`) VALUES
(18, 'jeff', 'obanda', 'jobanda@gamil', '', 28882, 'kii', '2018-05-10 11:07:34', 'jobanda', '$2y$10$dUSsSRY3Z1P1oKiKJidgm.57sNxZsQjsCX.c3owYjeRCOkq4jT/B.', ''),
(19, 'dan', 'obanda', 'dobanda@yahoo.com', '', 828829, 'kii', '2018-05-10 11:08:14', 'dan', '$2y$10$dUSsSRY3Z1P1oKiKJidgm.57sNxZsQjsCX.c3owYjeRCOkq4jT/B.', ''),
(21, 'Evans', 'Evans', 'Evans@gmail.com', 'avatar/Screenshot (3).png', 2147483647, 'Thisns', '2018-05-10 12:06:29', 'Evans', '$2y$10$dUSsSRY3Z1P1oKiKJidgm.57sNxZsQjsCX.c3owYjeRCOkq4jT/B.', ''),
(22, 'end', 'times', 'times@email', '', 0, '', '2018-05-11 08:55:55', 'times', '$2y$10$dUSsSRY3Z1P1oKiKJidgm.57sNxZsQjsCX.c3owYjeRCOkq4jT/B.', ''),
(23, 'Koech', 'Evans', 'EvansK@k.com', '', 0, '', '2018-05-11 13:15:24', 'EvansK', '$2y$10$dUSsSRY3Z1P1oKiKJidgm.57sNxZsQjsCX.c3owYjeRCOkq4jT/B.', ''),
(24, 'EvansK', 'EvansK', 'EvansK@k.com', '', 0, '', '2018-05-11 13:19:25', 'EvansK', '$2y$10$S3YLoM5xAdzt9MnlP53YSeXgFUMM46vFbFVwifdScSCFZSgEhLNrW', ''),
(25, 'EvansK', 'EvansK', 'EvansKa@h.co.ke', '', 0, '', '2018-05-11 13:20:14', 'EvansKa', '$2y$10$ZJ4n7TunSZh0ZrytI9o3h.K4lwuK.QE8ClJWqRxI8per/zstcD1cu', ''),
(26, 'Polycarp', 'Masika', 'masikapolycarp@gmail.com', '', 0, '', '2018-06-04 20:09:13', 'admin', '$2y$10$r.XDQBGwYZV/Fl5UtereZO86cutg49niYcWjwamjgCKmQ2CO4zWhi', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `posts_ibfk_1` (`userid`),
  ADD KEY `posts_ibfk_2` (`category_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `posts_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `categories` (`ID`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
