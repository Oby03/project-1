<?php
/**
 * Send message module
 */
require '../includes/dbconfig.php';
if (isset($_POST['message'])) {
    $from_id = isset($_POST['from_id']) ? $_POST['from_id'] : 0;
    $to_id = $_POST['to_id'];
    $message = mysqli_real_escape_string($dbconn, $_POST['message']);
    $sql = "
    INSERT INTO messages(from_userid, to_userid, message_text)
    VALUES (
        '$from_id',
        '$to_id',
        '$message'
    )";
    $query = mysqli_query($dbconn, $sql);
    if ($query) {
        // this executed as expected
        echo json_encode(['success' => true]);
    } else {
        echo json_encode(['success' => false, 'error' => mysqli_error($dbconn)]);
    }
}
?>