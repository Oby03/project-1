<?php
include_once("../includes/dbconfig.php");

// include_once('includes/header.php');
$username = $password = $firstname = $lastname = $email = "";
$username_err = $password_err = $fname_err = $lname_err = $email_err = "";

if(isset($_POST['create-admin'])){

    if(empty($_POST['fname'])){
        $fname_err ="Please Enter firstname";
    }else{
        $firstname =$_POST['fname'];
    }

    if(empty($_POST['lname'])){
        $lname_err ="Please enter your lastname";
    }else{
        $lastname =$_POST['lname'];
    }

    if(empty($_POST['username'])){
        $username_err ="Please enter Password";
    }else{
        $username =$_POST['username'];
    }

    if(empty($_POST['email'])){
        $email_err ="Please enter your email";
    }else{
        $email =$_POST['email'];
    }

    if(empty($_POST['password'])){
        $password_err ="Please enter Password";
    }else{
        $password =$_POST['password'];
    }

    if(empty($username_err) && empty($password_err) && empty($email_err) && empty($fname_err) && empty($lname_err)){
        $hashpass =password_hash($password, PASSWORD_DEFAULT);
        $sql = "INSERT INTO admin(User_Name, Password, email, firstname, lastname, created_date) VALUES('$username', '$hashpass', '$email', '$firstname', '$lastname', CURRENT_TIMESTAMP)";
        $stmt =mysqli_query($dbconn, $sql);
        if($stmt){
            session_start();
            $_SESSION['username'] = $stmt['User_Name'];
            header('location:dashboard.php');
        }else{
            echo mysqli_error($dbconn);
        }
    }
}
?>
<div class="col-md-6">
    <div class="add-admin">
                <form action="" method="post">
                <div class="form-group">
                    <input type="text" name="fname" id="" class="form-control" value="<?php echo $username;?>" placeholder="Firstname">
                    <span><?php echo $fname_err ?></span>
                </div>
                <div class="form-group">
                    <input type="text" name="lname" id=""  class="form-control" placeholder="Lastname">
                    <span><?php echo $lname_err; ?></span>
                </div>
                <div class="form-group">
                    <input type="text" name="username" id=""  class="form-control" placeholder="Username">
                    <span><?php echo $username_err; ?></span>
                </div>
                <div class="form-group">
                    <input type="email" name="email" id=""  class="form-control" placeholder="Email">
                    <span><?php echo $email_err; ?></span>
                </div>
                <div class="form-group">
                    <input type="password" name="password" id=""  class="form-control" placeholder="Enter Password">
                    <span><?php echo $password_err; ?></span>
                </div>
                <button type="submit" name="create-admin" class="btn btn-primary">Create Admin</button>
        </form>
    </div>
        
</div>

</div>