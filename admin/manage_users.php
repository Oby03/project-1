<?php 
include_once("../includes/dbconfig.php");
// include_once("includes/header.php");
?>
<link href="assets/css/dataTables/dataTables.bootstrap.css" rel="stylesheet">
<script src="assets/js/dataTables/jquery.dataTables.min.js"></script>
        <script src="assets/js/dataTables/dataTables.bootstrap.min.js"></script>

        <!-- DataTables Responsive CSS -->
        <link href="assets/css/dataTables/dataTables.responsive.css" rel="stylesheet">
<div class="col-lg-8">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                All Users
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>First Name</th>
                                                <th>Last Name</th>
                                                <th>Username</th>
                                                <th>Email</th>
                                                <th>location</th>
                                                <th>Phone</th>
                                                <th>Short Bio</th>
                                                <th>Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                                <?php 
                                                $sql =mysqli_query($dbconn, "SELECT * FROM users");

                                                if($sql){
                                                while($stmt =mysqli_fetch_assoc($sql)){
                                                ?>
                                            <tr>
          
                                                <td><?php echo $stmt['firstname'];?></td>
                                                <td><?php echo $stmt['lastname'];?></td>
                                                <td><?php echo $stmt['User_Name'];?></td>
                                                <td><?php echo $stmt['email'];?></td>
                                                <td><?php echo $stmt['location'];?></td>
                                                <td><?php echo $stmt['phone'];?></td>
                                                <td><?php echo $stmt['shortbio'];?></td>
                                                <td><a href="deleteuser.php?id=<?php echo $stmt['ID']; ?>"><button class="btn btn-danger">Delete</button></a></td>                                           
                                            </tr>

                                                <?php
                                                    }
                                                    }else{
                                                    echo "No results found";
                                                    }
                                                    ?>
                                            
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.table-responsive -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
