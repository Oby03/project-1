<?php 
// session_start();
include_once('../includes/dbconfig.php');
$userid = $_SESSION['userid'];

$sql =mysqli_query($dbconn, "SELECT * FROM admin WHERE ID=$userid");
$stmt =mysqli_fetch_assoc($sql);
$hashpass =$stmt['Password'];

$password =$newpass ="";
$password_error =$newpass_error ="";
if(isset($_POST['update'])){

    if(empty($_POST['password'])){
        $password_error = "Please Enter Old Password";
    }else{
        $password =trim($_POST['password']);
    }

    if(empty($_POST['newpass'])){
        $newpass_error = "Please Enter Old Password";
    }else{
        $newpass =trim($_POST['newpass']);
    }

    echo $password . $newpass;
    if(empty($password_error) && $newpass_error){
        if(password_verify($password, $hashpass)){
            $hash = password_hash($newpass,PASSWORD_DEFAULT);
         $insert =mysqli_query($dbconn,"UPDATE admin SET Password = '$hash'");
         if($insert){
             header("Location:index.php");
         }else{
             echo mysqli_errror($dbconn);
         }
        }
    }
}
?>
<div class="row">
<div class="col-md-4"></div>
<div class="col-md-4">
<h3>Add admin</h3>

<form action="" method="post">
    <div class="form-group">
    <input type="text" name="name" id="" class="form-control" disabled value="<?php echo $stmt['User_Name'];?>">
    </div>
    <div class="form-group">
    <label for="Old password">Old Password</label>
    <input type="password" name="password" id="" class="form-control">
    <span><?php echo $password_error ?></span>
    </div>
    <div class="form-group">
    
    </div>
    <div class="form-group">
    <label for="new password">New Password</label>
    <input type="password" name="newpass" id="" class="form-control">
    <span><?php echo $newpass_error ?></span>
    </div>
    <button type="submit" name="update" class="btn btn-primary">Update</button>
</form>

</div>
<div class="col-md-4"></div>
</div>
