<?php 

include_once('../includes/dbconfig.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Adshare</title>
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/main.css">
    <script src="assets/js/jquery-3.3.1.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    
</head>
<body>

<div class="header ">
    <nav class="navbar navbar-inverse" role="navigation">
        <div class="navbar-header"> 
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#example-navbar-collapse"> 
            <span class="sr-only">Adverts</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button> 
            <a class="navbar-brand" href="dashboard.php">Adshare</a>
            </div>
        <div class="collapse navbar-collapse" id="example-navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="dashboard.php">Home</a></li>
                    <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <?php echo $_SESSION['username'];?> <b class="caret"></b> </a> 
                        <ul class="dropdown-menu">
                            <li><a href="">Profile</a></a></li>
                            <li><a href="">Log Out</a></li>
                            <li><a href="#">Item 2</a></li> 
                        </ul> 
                    </li>
                </ul>
                <form class="navbar-form navbar-left" role="search" method="post" autocomplete="off" action="search.php">
                    <div class="form-group">
                        <input type="text" class="search form-control" id="searchbox" placeholder="Search for People" name="filter"/><br />
                        <div id="display"></div>
                    </div> 
                    <button type="submit" class="btn btn-default btn-success">Search</button>
				</form> 
        </div> 
    </nav>
</div>
