<?php
session_start();
if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
    header("location:signin.php");
}?>
<?php
include_once("../includes/dbconfig.php");
$id = $_GET['id'];
$sql = "DELETE FROM users WHERE ID = $id ";

if (mysqli_query($dbconn, $sql)) {
    echo "User deleted successfully";
} else {
    echo "Error deleting record: " . mysqli_error($dbconn);
}

mysqli_close($dbconn);

header("location:dashboard.php?active=users#");
?>