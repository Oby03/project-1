<?php 
include_once("../includes/dbconfig.php");
// include_once("includes/header.php");
?>

<link href="assets/css/dataTables/dataTables.bootstrap.css" rel="stylesheet">
<script src="assets/js/dataTables/jquery.dataTables.min.js"></script>
        <script src="assets/js/dataTables/dataTables.bootstrap.min.js"></script>

        <!-- DataTables Responsive CSS -->
        <link href="assets/css/dataTables/dataTables.responsive.css" rel="stylesheet">
<div class="col-lg-8">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                All Posts
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Title</th>
                                                <th>Photo</th>
                                                <th>Description</th>
                                                <th>Category</th>
                                                <th>User</th>
                                                <th>Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                               <?php 
                                                    $sql =mysqli_query($dbconn, "SELECT * FROM posts");

                                                    if($sql){
                                                    while($stmt =mysqli_fetch_assoc($sql)){
                                                ?>
                                                <tr>
                                                <td><?php echo $stmt['Title'];?></td>
                                                <td><img src="<?php echo $stmt['Photo'];?>" alt=""></td>
                                                <td><?php echo $stmt['Description'];?></td>
                                                <td><?php echo $stmt['category_id'];?></td>
                                                <td><?php echo $stmt['userid'];?></td>
                                                <td><a href="delete.php?id=<?php echo $stmt['ID']; ?>"><button class="btn btn-danger">Delete</button></a></td>
                                                </tr>

                                                <?php
                                                    }
                                                    }else{
                                                    echo "No results found";
                                                    }
                                                    ?>
                                            
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.table-responsive -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
