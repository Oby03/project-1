<?php
session_start();
if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
    header("location:signin.php");
}?>
<?php
include_once("includes/header.php");
$id = $_GET['ID'];
$sql = "DELETE FROM posts WHERE ID = $id ";

if (mysqli_query($dbconn, $sql)) {
    echo "Record deleted successfully";
} else {
    echo "Error deleting record: " . mysqli_error($dbconn);
}


mysqli_close($dbconn);

header("location=view_edit_post.php");
