
<link href="assets/css/font-awesome.min.css" rel="stylesheet">
<link rel="stylesheet" href="assets/css/bootstrap.min.css">
<script src="assets/js/jquery-3.3.1.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/main.js"></script>
<link rel="stylesheet" href="assets/css/main.css">

<?php
include_once('includes/dbconfig.php');
// include_once('includes/header.php');

$sql = "SELECT COUNT(*) FROM posts";
$result = mysqli_query( $dbconn,$sql);

		$r = mysqli_fetch_row($result);
		// var_dump($r);
		// echo $r[0];

$numrows = $r[0];

$rowsperpage = 5;

$totalpages = ceil($numrows / $rowsperpage);

// echo $totalpages;

if (isset($_GET['currentpage']) && is_numeric($_GET['currentpage'])) {
		// cast var as int
		$currentpage = (int) $_GET['currentpage'];
 } else {
		// default page num
		$currentpage = 1;
 } 


 if ($currentpage > $totalpages) {
		
		$currentpage = $totalpages;
 } 
 
 if ($currentpage < 1) {

		$currentpage = 1;
 } 

 $offset = ($currentpage - 1) * $rowsperpage;

 $sql =( "SELECT posts.*, users.User_Name FROM posts INNER JOIN users ON users.ID =posts.userid  ORDER BY TIME DESC LIMIT $offset, $rowsperpage");

$result = mysqli_query($dbconn, $sql);

// var_dump($result);
while ($stmt = mysqli_fetch_assoc($result)) {

		?>
<div class="row">
	<div class="col-md-4">
		<div class="control-panel"></div>
	</div>
	<div class="col-md-7">
		<div id="adv" style="height: 302px;  padding:2px; background: white; border-radius: 15px; margin-bottom: 5px;">
				<div class="row">
					<div class="col-md-4" >

						<img class="img-thumbnail " src="<?php echo $stmt['Photo']; ?>" alt="placeholder" style="height: 250px; margin-right:  0px;">
					
						<div class="col-md-12">
							<span><?php echo $stmt['Time']?></span>
						</div>
				 </div>
		<div class="col-md-6" style="margin-left: 0;">
					<div class="row" class="title" style="height:40px; padding:5px;">
						<p><strong><?php 
						echo $stmt['Title'];?></strong></p>
						<p><?php echo $stmt['Description'];?></p>
						<div class="col-xs-12">
							<b>Posted by: @<?php echo $stmt['User_Name'] ;?></b></h3>
						</div>
						<div class="col-xs-6">
							<!-- Button trigger modal -->
							<button
								type="button"
								class="btn btn-primary btn-lg modalTrigger"
								data-user-id="<?php echo $stmt['userid']; ?>"
								data-user-name="<?php echo $stmt['User_Name'] ;?>"
								>
							Send Message
							</button>
						</div>
						<div class="col-xs-6">
							<a
								href="https://www.facebook.com/sharer/sharer.php?s=100&p[url]=http://www.example.com&p[images][0]=&p[title]=Title%20Goes%20Here&p[summary]=Description%20goes%20here!"
								target="_blank"
								onclick="window.open(this.href,'targetWindow','toolbar=no,location=0,status=no,menubar=no,scrollbars=yes,resizable=yes,width=600,height=250'); return false">
									<button
									type="button"
									class="btn btn-facebook btn-lg">
									<i class="fa fa-facebook fa-2">
									</i> Share on Facebook</button></a>
						</div>
					</div>
		</div>    
	</div>
</div>
	</div>
	<div class="col-md-1"></div>
</div>
<!-- bootstrap modal -->
<div class="modal fade" id="messageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Send Message to @<span class="user_name"></span></h4>
      </div>
      <div class="modal-body">
	  	<div class="alert alert-success" id="message-success">
		  <p> Message Sent Successfully! </p>
		</div>
        <form id="sendForm">
          <div class="form-group">
            <label for="recipient-name" class="control-label">Recipient:</label>
            <input type="text" name="to_name" class="form-control to_name" id="recipient-name" disabled>
			<input type="hidden" name="to_id" class="to_id"/>
          </div>
          <div class="form-group">
            <label for="message-text" class="control-label">Message:</label>
            <textarea
				class="form-control message_text"
				name="message"
				id="message-text"
				>
				</textarea>
			<i>Remember to include your contact information.</i>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="sendMessage">Send</button>
      </div>
    </div>
  </div>
</div>







		<?php

 } 
 
 /******  build the pagination links ******/
 // range of num links to show
 $range = 3;
 
 // if not on page 1, don't show back links
 if ($currentpage > 1) {
		// show << link to go back to page 1"
		echo ' <a href="{"$_SERVER[PHP_SELF"]}?currentpage=1"><</a>';
		// get previous page num
		$prevpage = $currentpage - 1;
		// show < link to go back to 1 page
		echo " <a href='{$_SERVER['PHP_SELF']}?currentpage=$prevpage'><</a> ";
 } // end if 
 
 // loop to show links to range of pages around current page
 for ($x = ($currentpage - $range); $x < (($currentpage + $range) + 1); $x++) {
		// if it's a valid page number...
		if (($x > 0) && ($x <= $totalpages)) {
			 // if we're on current page...
			 if ($x == $currentpage) {
					// 'highlight' it but don't make a link
					echo " [<b class='pagination'>$x</b>] ";
			 // if not current page...
			 } else {
					// make it a link
					echo " <a href='{$_SERVER['PHP_SELF']}?currentpage=$x'>$x</a> ";
			 } // end else
		} // end if 
 } // end for
									
 // if not on last page, show forward and last page links        
 if ($currentpage != $totalpages) {
		// get next page
		$nextpage = $currentpage + 1;
		 // echo forward link for next page 
		echo " <a href='{$_SERVER['PHP_SELF']}?currentpage=$nextpage'>></a> ";
		// echo forward link for lastpage
		echo " <a href='{$_SERVER['PHP_SELF']}?currentpage=$totalpages'>>></a> ";
 } // end if
 /****** end build pagination links ******/

?>

<!-- include_once("includes/footer.php"); -->

<?php  ?>            