<?php

// session_start();
include_once('includes/dbconfig.php');
$category = $title =$description = $photo="";
$category_err = $title_err =$description_err = $photo_err ="";

if(isset($_POST['post'])){

    // if(empty($_POST['category'])){
    //     $category_err = "Please Enter ";
    // }else{
    //     $category = trim($_POST['category']);
    // }
    $category = trim($_POST['category']);

    if(empty($_POST['title'])){
        $title_err = "Please Enter ";
    }else{
        $title = trim($_POST['title']);
    }
    if(empty($_POST['description'])){
        $description_err = "Please Enter ";
    }else{
        $description = trim($_POST['description']);
    }
$photo = $_FILES['photo'];
// $photo = $_POST['photo'];
$userid = $_SESSION['userid'];
echo $userid;

$photo_err =$_FILES["photo"]["error"];
$filename = $_FILES["photo"]["name"];
$filetype = $_FILES["photo"]["type"];
$filesize = $_FILES["photo"]["size"];
$path ="uploads/" . $_FILES["photo"]["name"];

    if(isset($photo) && $photo_err == 0){
        $allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "gif" => "image/gif", "png" => "image/png");
   
        // Verify file extension
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        if(!array_key_exists($ext, $allowed)) die("Error: Please select a valid file format.");

        // Verify file size - 5MB maximum
        $maxsize = 7 * 1024 * 1024;
        if($filesize > $maxsize) die("Error: File size is larger than the allowed limit.");
    
        // Verify MYME type of the file
        if(in_array($filetype, $allowed)){

            // Check whether file exists before uploading it
            $path ="uploads/" . $_FILES["photo"]["name"];   
            if(file_exists($path)){
                echo $_FILES["photo"]["name"] . " it already exists.";
                return;
            } else{
                move_uploaded_file($_FILES["photo"]["tmp_name"],$path);
                
                echo "Your file was uploaded successfully.";
            } 
        } else{
            echo "Error: There was a problem uploading your file. Please try again."; 
        }
    } else{
        echo "Error: " . $_FILES["photo"]["error"];
    }
    
    if(empty($category_err) && empty($title_err) && empty($photo_err) && empty($description_err)){

    //  var_dump(   $category . $title . $description . $path);
        $sql = mysqli_query($dbconn, "INSERT INTO posts(Title, Photo , Description, category_id, userid) VALUES('$title', '$path', '$description', '$category', $userid)");

        if($sql){
            header("location:home.php");

            echo "Post Success";
        }else{
            echo mysqli_error($dbconn);
        }
    }
}
$sql1 =mysqli_query($dbconn, "SELECT * FROM categories");
?>

<div class="row">

    
        <div id="addpost">
            <h4>Add Post </h4>

        <form action="" method="post" enctype="multipart/form-data" >
            <div class="form-group">
                <select name="category" id="" class="form-control">
<?php while($stmt =mysqli_fetch_assoc($sql1))
{
   echo '<option value="'.$stmt['ID'].'">'.$stmt['Category_Name'].' </option>';

}?>                    <span><?php echo $category_err; ?></span>   
                </select>
            </div>
            <div class="form-group float-label-control">
                <label for="">Title</label>
                <input type="text" name="title" id="" class="form-control" placeholder="Please Enter The title of your advert">
                <span><?php echo $title_err; ?></span>
            </div>
            <div class="form-group float-label-control">
                <label for="">Ad Media</label>
                <center><input type="file" name="photo" id="" class="form-control"></center>
                <span><?php echo $photo_err; ?></span>
            </div>
            <div class="form-group float-label-control">
                <label for="">Ad Description</label>
                <textarea name="description" id="" cols="30" rows="10" class="form-control"></textarea>
                <span><?php echo $description_err; ?></span>            
            </div>
            <button type="submit" name="post" class="btn btn-primary">Post</button>
        </form>
        </div>
            
</div>
