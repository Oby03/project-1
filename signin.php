<?php 
include_once('includes/dbconfig.php');
include_once('includes/header-index.php');
$username = $password ="";
$username_err = $password_err ="";

if(isset($_POST['signin'])){
    if(empty($_POST['username'])){
        $username_err ="Please Enter User Name";
    }else{
        $username =$_POST['username'];
    }
    if(empty($_POST['password'])){
        $password_err ="Please enter Password";
    }else{
        $password =$_POST['password'];
    }
    if(empty($username_err) && empty($password_err)){

        $sql = mysqli_query($dbconn, "SELECT * FROM users WHERE User_Name ='$username'");
        $stmt =mysqli_fetch_assoc($sql);
       $hashed_password = $stmt['Password'];

        if(password_verify($password, $hashed_password )){
            session_start();
            $_SESSION['userid'] = $stmt['ID'];
            $_SESSION['username'] = $stmt['User_Name'];
            header('location:home.php');
         
        }else{
            echo "Password Wrong";
        }
    }
}
?>

<div class="row">
<div class="col-md-4"></div>
<div class="col-md-4">
    <div id="signin">
        <h3>AdShare | Signin</h3>
        <form action="" method="post">
                <div class="form-group">
                    <input type="text" name="username" id="" class="form-control" value="<?php echo $username  ?>" placeholder="Enter User Name">
                    <span><?php echo $username_err ?></span>
                </div>
                <div class="form-group">
                    <input type="password" name="password" id="" class="form-control" placeholder="Enter Password">
                    <span><?php echo $password_err ?></span>
                </div>
                <button type="submit" name="signin" class="btn btn-primary" >Sign In</button>
                <div class="dont" style="padding-top:10px;">
                    Don't have an account yet? <a href="signup.php" class="link" style=" color:blue;">Sign up here...</a>
                </div>
                
            </form>
    </div>   
</div>
<div class="col-md-4"></div>
</div>

